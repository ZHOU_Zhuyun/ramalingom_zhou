import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class GUIConnectionPanel extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private GUI mainFrame;
	
	//Paneaux pour le bouton Connection
	private JPanel pCo;

	private JLabel  jlId    = new JLabel("Identifiant");   //Initialiser la variable "graphique"
    private JLabel  jlMdp = new JLabel("Mot de Passe");
    
    private JTextField textUtilisateur;
    private JTextField textMdp;
    
    private JButton boutonConnection;
    
    /**
     * Paramettre la JDBC 
     * user : ChefMaintenance  pass: maintenance
     * user : admin			   pass: pdl
     * user : Operateur		   pass: operateur
     */
    final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";// final static indique que les valeurs ne peuvent �tre chang�es
     String LOGIN="****";
	 String PASS="****";
	Connection con=null;
	
    /**
     * Constructeur du panneau Connection 
     * @param mainFrame qui recup�re l'instance de fen�tre en cours
     *
     */
    public GUIConnectionPanel(GUI mainFrame){
    	this.mainFrame=mainFrame;
    	    	
    	textUtilisateur=new JTextField();
    	textMdp=new JTextField();
    	boutonConnection=new JButton("Connection");
    	this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
    	pCo=new JPanel();
    	pCo.setLayout(new BorderLayout());
    	
    	this.add(Box.createRigidArea(new Dimension(0, 50)));
    	this.add(jlId);   // Ajouter le composant "nom" au panel
    	this.add(Box.createRigidArea(new Dimension(0, 5)));
    	this.add(textUtilisateur);
    	this.add(Box.createRigidArea(new Dimension(0, 50)));
    	
    	this.add(jlMdp);
    	this.add(Box.createRigidArea(new Dimension(0, 5)));
    	this.add(textMdp);
    	this.add(Box.createRigidArea(new Dimension(0, 50)));
    	
    	
    	pCo.add(boutonConnection, BorderLayout.PAGE_END);
    	this.add(Box.createRigidArea(new Dimension(0, 5)));
    	
    	this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
    	
    	boutonConnection.addActionListener(this);
    	this.add(pCo);
    }
	   
	
	/**
     * Diff�rents getteur
     * @return
     */
	public JLabel getJlId() {
		return jlId;
	}
	
	public JLabel getJlMdp() {
		return jlMdp;
	}
	/**
	 * Configuration de l'action des boutons en fonction de la connection a la base de donn�e
	 */
	
	public void actionPerformed(ActionEvent arg0) {
		try {
			/**
			 * Ici il faut importer le fichier .jar dans le dossier Lib et egalement dans eclipse dans les library 
			 */
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
		} catch (ClassNotFoundException e) {
			System.err
					.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
		
		try {			
			
			this.LOGIN =this.textUtilisateur.getText();
			this.PASS=this.textMdp.getText();
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			if (this.LOGIN.equals("ChefMaintenance")){
				this.mainFrame.switchPanel("Chef de Maintenance");
			}
			if (this.LOGIN.equals("Operateur")){
				this.mainFrame.switchPanel("Menu operateur");
			}
			
			if (this.LOGIN.equals("admin")){
				this.mainFrame.switchPanel("Admin");
			}
		} catch (SQLException e) {
			
			//e.printStackTrace();
			System.out.println("invalid username/password; logon denied");
		}
		
		
	}

}
