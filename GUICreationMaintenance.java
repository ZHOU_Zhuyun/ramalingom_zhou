import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;

/**
 * 
 * @author yuyu
 * @version 1.0
 */

public class GUICreationMaintenance extends JPanel  implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private GUI mainFrame;
	
	//Paneaux pour le bouton enregitrer
			private JPanel pEnregistrer;
	//Paneaux pour le fond
			private JPanel pFond;
	
	//private JLabel  jlId = new JLabel("");
    private JLabel  jlNom    = new JLabel("Nom");   //Initialiser la variable "graphique"
    private JLabel  jlType = new JLabel("Type");
    private JLabel  jlDate = new JLabel("Date");
    private JLabel  jlProgression = new JLabel("Progression");
    private JLabel  jlCommentaire = new JLabel("Commentaire");
    
    //private JTextField textId;
    private JTextField textNom;
    private JTextField textType;
    private JTextField textDate;
    private JTextField textProgression;
    private JTextField textCommentaire;
    
    private JButton boutonEnregistrer;
   // private JButton boutonRetour;
    
    public GUICreationMaintenance(GUI mainFrame) {
    	this.mainFrame=mainFrame;
	
    	this.setLayout(new BorderLayout());;//.setLayout(new FlowLayout());   //Disposition des éléments composants
	//textId=new JTextField();
	textNom=new JTextField();
	textType=new JTextField();
	textDate=new JTextField();
	textProgression=new JTextField();
	textCommentaire=new JTextField();
	boutonEnregistrer=new JButton("Enregistrer");
	//boutonRetour=new JButton("Retour");
	
	pFond=new JPanel();
	pFond.setLayout(new BoxLayout(pFond,BoxLayout.PAGE_AXIS));
	
	pEnregistrer=new JPanel();
	pEnregistrer.setLayout(new BorderLayout());
	
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	pFond.add(jlNom);   // Ajouter le composant "nom" au panel
	pFond.add(Box.createRigidArea(new Dimension(0, 5)));
	pFond.add(textNom);
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	
	pFond.add(jlType);
	pFond.add(Box.createRigidArea(new Dimension(0, 5)));
	pFond.add(textType);
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	
	pFond.add(jlDate);
	pFond.add(Box.createRigidArea(new Dimension(0, 5)));
	pFond.add(textDate);
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	
	pFond.add(jlProgression);
	pFond.add(Box.createRigidArea(new Dimension(0, 5)));
	pFond.add(textProgression);
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	
	pFond.add(jlCommentaire);
	pFond.add(Box.createRigidArea(new Dimension(0, 5)));
	pFond.add(textCommentaire);
	pFond.add(Box.createRigidArea(new Dimension(0, 10)));
	
	
	
	
	pEnregistrer.add(boutonEnregistrer, BorderLayout.PAGE_END);
	//this.add(Box.createRigidArea(new Dimension(0, 5)));
	//this.add(boutonRetour);
	//this.add(Box.createRigidArea(new Dimension(0, 5)));
	
	this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	
	boutonEnregistrer.addActionListener(this);
	//boutonRetour.addActionListener(this);
	
	this.add(pFond, BorderLayout.CENTER);
	this.add(pEnregistrer, BorderLayout.PAGE_END);

    }

    /**
     * 
     * @return
     */
	public JLabel getJlNom() {
		return jlNom;
	}
	
	public JLabel getJlType() {
		return jlType;
	}
	
	public JLabel getJlDate() {
		return jlDate;
	}
	
	public JLabel getJlProgression() {
		return jlProgression;
	}
	
	public JLabel getJlCommentaire() {
		return jlCommentaire;
	}
	
	public void actionPerformed(ActionEvent ae) {

		
		if (ae.getSource() == boutonEnregistrer) {
			//this.mainFrame.switchPanel("Menu operateur");
		}
		/*else if (ae.getSource()==boutonRetour){
			this.mainFrame.switchPanel(this.mainFrame.getRetour());
		}*/
		
	}	
	
}