/**
 * 
 * @author yuyu
 * @version 1.1
 */
public class Personne {
	
	protected String nom;
	protected String id;
	protected String mdp;
	protected String numTel;
	
	/**
	 * @param nom
	 * @param id
	 * @param mdp
	 * @param numTel
	 */
	public Personne(String nom, String id, String mdp, String numTel) {
		super();
		this.nom = nom;
		this.id = id;
		this.mdp = mdp;
		this.numTel = numTel;
	}
	
	/**
	 * @param id
	 * @param mdp
	 */
	public Personne(String id, String mdp) {
		super();
		this.id = id;
		this.mdp = mdp;
	}

	/**
	 * @param mdp the mdp to set
	 */
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	/**
	 * @param numTel the numTel to set
	 */
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMdp() {
		return mdp;
	}

	public String getNumTel() {
		return numTel;
	}

	public boolean sAuthentifier(String identifiant,String motPasse){
		if (identifiant.equals(id)){
			if (motPasse.equals(mdp)){
				return true;
			}
			else{
				System.out.println("mot de passe inconu!");
				return false;
			}
		}
		else{
			System.out.println("ID inconnu!");
			return false;
		}
			
			
			
	}
	
}
