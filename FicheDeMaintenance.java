import java.sql.Date;

/**
 * @author yuyu
 * @version 1.0
 */
public class FicheDeMaintenance {
	
	private String nomOp;
	private String typeMaintenance;
	private Date date;
	private String progression;
	private String commentaire;
	
	/**
	 * @param nomOp
	 * @param typeMaintenance
	 * @param date
	 * @param progression
	 * @param commentaire
	 */
	public FicheDeMaintenance(String nomOp, String typeMaintenance, Date date, String progression, String commentaire) {
		this.nomOp = nomOp;
		this.typeMaintenance = typeMaintenance;
		this.date = date;
		this.progression = progression;
		this.commentaire = commentaire;
	}

	/**
	 * @return the nomOp
	 */
	public String getNomOp() {
		return nomOp;
	}

	/**
	 * @return the typeMaintenance
	 */
	public String getTypeMaintenance() {
		return typeMaintenance;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @return the progression
	 */
	public String getProgression() {
		return progression;
	}

	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}
	
	/**
	 * @param dateModif
	 */
	public void modifierFiche(Date dateModif) {
		date = dateModif;
	}

}
