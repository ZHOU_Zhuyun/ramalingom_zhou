import java.sql.*;
import java.util.ArrayList;
import java.util.List;
public class OperateurDAO {
	
	 final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	 final static String LOGIN = "admin";  //test
		final static String PASS = "pdl";   

	
	 public OperateurDAO(){
		 try {
				Class.forName("oracle.jdbc.OracleDriver");
			} catch (ClassNotFoundException e) {
				System.err
						.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}
	 }
	 
	 public void ajoutOperateur(Operateur o){
		 	Connection con = null;
			PreparedStatement ps = null;
			int retour = 0;

			// connexion a� la base de donn�es
			try {

				// tentative de connexion
				con = DriverManager.getConnection(URL,LOGIN,PASS);
				// préparation de l'instruction SQL, chaque ? représente une valeur
				// à communiquer dans l'insertion
				// les getters permettent de récupérer les valeurs des attributs
				// souhaités
				ps = con.prepareStatement("INSERT INTO Operateur (Nom, Id_operateur, Mot_de_passe, Num_telephone) VALUES (?, ?, ?, ?)");
				ps.setString(1, o.getNom());
				ps.setString(2, o.getId());
				ps.setString(3, o.getMdp());
				ps.setString(4, o.getNumTel());

				// Exécution de la requête
				retour = ps.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				// fermeture du preparedStatement et de la connexion
				try {
					if (ps != null)
						ps.close();
				} catch (Exception ignore) {
				}
				try {
					if (con != null)
						con.close();
				} catch (Exception ignore) {
				}
			}
	 }

}
