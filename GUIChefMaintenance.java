import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import java.awt.GridLayout;

import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;
/**
 * Panel du Menu du chef de maintenance 
 * @author christophe
 *
 */
public class GUIChefMaintenance extends JPanel  implements ActionListener {
	private static final long serialVersionUID = 1L;

	private GUI mainFrame;
	
	//Paneaux pour le bouton deconnexion
		private JPanel pCo;
		
	//Panneaux pour coller les panneaux
		private JPanel pFond;
	
	/**
	 * Cr�ation des diff�rents bouton du panel
	 */
	private JButton boutonSaisieClient;
	private JButton boutonSaisieMaintenance;
	private JButton boutonMenuOp;
	private JButton boutonReporting;
	private JButton boutonDeconnexion;

	
	 public GUIChefMaintenance(GUI mainFrame) {
		 this.mainFrame=mainFrame;
		 this.setLayout(new BorderLayout());
		// this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
			//this.setBackground(Color.); couleur arri�re plan //Sert a changer la couleur de l'arri�re plan
			
			pFond=new JPanel();
			pFond.setLayout(new GridLayout( 2, 2, 5, 5));
			pCo=new JPanel();
			pCo.setLayout(new BorderLayout());
			
			boutonSaisieClient=new JButton("Menu Saisie Client");
			boutonSaisieMaintenance=new JButton("Menu Saisie Maintenance");
			boutonMenuOp=new JButton("Menu Op�rateur");
			boutonReporting=new JButton("Reporting");
			boutonDeconnexion=new JButton("D�connexion");
			
			
			
			pFond.add(boutonSaisieClient);
			//pFond.add(Box.createRigidArea(new Dimension(0, 50)));
			pFond.add(boutonSaisieMaintenance);
			//pFond.add(Box.createRigidArea(new Dimension(0, 50)));
			pFond.add(boutonMenuOp);
			//pFond.add(Box.createRigidArea(new Dimension(0, 50)));
			pFond.add(boutonReporting);
			pCo.add(Box.createRigidArea(new Dimension(0, 50)));
			
			pCo.add(boutonDeconnexion, BorderLayout.PAGE_END);
			
			
			
			
			boutonSaisieClient.addActionListener(this);
			boutonSaisieMaintenance.addActionListener(this);
			boutonMenuOp.addActionListener(this);
			boutonReporting.addActionListener(this);
			boutonDeconnexion.addActionListener(this);
			
			
			this.add(pFond, BorderLayout.CENTER);
			this.add(pCo, BorderLayout.PAGE_END);
			this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20,20));
	 }
	
	
	public void actionPerformed(ActionEvent ae) {
		/**
		 * D�finie cette fen�tre comme la fen�tre pr�cedente
		 */
	
		this.mainFrame.setRetour("Chef de Maintenance");
		
		if (ae.getSource() == boutonMenuOp) {
			this.mainFrame.switchPanel("Menu operateur");
		}
		else if (ae.getSource()==boutonSaisieClient){
			this.mainFrame.switchPanel("Menu saisie client");
		}
		else if(ae.getSource()==boutonSaisieMaintenance){
			this.mainFrame.switchPanel("Menu saisie maintenance");
		}
		else if(ae.getSource()==boutonReporting){
			this.mainFrame.switchPanel("connection");
		}
		else if(ae.getSource()==boutonDeconnexion){
			this.mainFrame.switchPanel("connection");
		}

	}	

}
