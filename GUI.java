import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;
import java.util.ArrayList;




public class GUI extends JFrame {
	private static final long serialVersionUID = 1L;
	/**
	 * Gui Principal avec la methode permetant de changer de panneaux
	 */
	
	private String retour="**";
	private ArrayList<String> listRetour=new ArrayList<String>();
	
	/**
	 * Attribut des panneaux	
	 * @attribute
	 * 
	 */
	private GUIChefMaintenance pChef;
	private GUIConnectionPanel pConnection;
	private GUIOperateur pOperateur;
	private GUIClient pClient;
	private GUIMaintenance pMaintenance;
	private GUICreationMaintenance pCreaMaintenance;
	private GUIAdmin pAdmin;
	
	/**
	 * costructeur de la fen�tre graphique
	 */
    public GUI() { 	    	
	this.setTitle("LOG IN");   // Titre de la fenêtre
	this.setSize(600, 600);   // Taille de la fenêtre
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// Fermeture du programme en fermant la fenêtre
	this.setLocationRelativeTo(null);   // Centrer la fenêtre
	/** Instanciation des diff�rents paneaux
	 */
		pConnection=new GUIConnectionPanel(this);
		pChef=new GUIChefMaintenance(this);
		pOperateur=new GUIOperateur(this);
		pClient=new GUIClient(this);
		pMaintenance=new GUIMaintenance(this);
		pCreaMaintenance=new GUICreationMaintenance(this);
		pAdmin= new GUIAdmin(this);
	
	this.setContentPane(pConnection);   // Ajouter le de connection panel a la fen�tre
	this.setVisible(true);   // Visualiser la fen�tre
    }
    
    public void setRetour(String retour){
    	this.retour=retour;
    	this.listRetour.add(retour);
    }
     public String getRetour(){
    	 String temp=listRetour.get(listRetour.size()-1);
    	 listRetour.remove(listRetour.size()-1);
    	 return temp; 
     }
     
     public GUICreationMaintenance getPanelCrea(){
    	 return pCreaMaintenance;
     }

    /**
     * methode permetant de changer de panneaux
     * @param nomPanneaux qui prend le nom du panneau a changer
     */
    public void  switchPanel(String nomPanneaux){
    	if(nomPanneaux.equals("Chef de Maintenance")){
    		this.setTitle("Menu Chef de Maintenance");   // Titre de la fenêtre
    		this.setContentPane(pChef);
    		this.revalidate();
       	}else if(nomPanneaux.equals("Menu operateur")){
       		this.setTitle("Menu Op�rateur");   // Titre de la fenêtre
       		this.setContentPane(pOperateur);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("Menu saisie client")){
       		this.setTitle("Gestion Client");   // Titre de la fenêtre
       		this.setContentPane(pClient);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("Menu saisie maintenance")){
       		this.setTitle("Menu Saisie Maintenance");   // Titre de la fenêtre
       		this.setContentPane(pMaintenance);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("connection")){
       		this.setTitle("LOG IN");
       		this.setContentPane(pConnection);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("Creation Maintenance")){
       		this.setTitle("Cr�ation d'une Maintenance");
       		this.setContentPane(pCreaMaintenance);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("Admin")){
       		this.setTitle("Menu Administrateur");
       		this.setContentPane(pAdmin);
       		this.revalidate();
       		
       	}else if(nomPanneaux.equals("")){
       		
       		
       	}
       	else if(nomPanneaux.equals("connection")){
       		this.setTitle("LOG IN");
       		this.setContentPane(pConnection);
    		this.revalidate();
       		
       	}else if(nomPanneaux.equals("connection")){
       		this.setTitle("LOG IN");
       		this.setContentPane(pConnection);
    		this.revalidate();
       		
       	}

    	
    }
    
	
	}
