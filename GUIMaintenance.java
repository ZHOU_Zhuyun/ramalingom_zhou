
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.Dimension;

import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import java.awt.GridLayout;

import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
/**Classe qui permet de cr�e un panel qui permet de Cr�er, supprimer ou faire de la gestion de maintenance
 * 
 * @author christophe-Zhuyun
 *
 */
public class GUIMaintenance extends JPanel implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	private GUI mainFrame;
    /**
     * Cr�ation des diff�rents Boutons  
     */
    private JButton boutonCreation;
    private JButton boutonRecherche;
    private JButton boutonSupprimer;
    private JButton boutonRetour;
    
    /**
     * Cr�ations des diff�rents panel 
     */
    private JPanel pOngletGestion;
    //private GUICreationMaintenance pCrea;
	private JPanel pOngletSupp;
	private GUICreationMaintenance pOngletCrea;
	    
	private JPanel pZoneTableau;
	private JPanel pRecherche;
	private JPanel pRetour;
	
    /**
     * Contener des onglets
     */
	private JTabbedPane tabbedPane;
	
	/**
	 * tableau des maintenance  
	 */
	private JTable tableau;
	
	/**
	 * Labels
	 */
	 private JLabel  jlRecherche = new JLabel("Entrer la Maintenance � rechercher");
	
	 /**
		 * zone de texte pour la recherche de maintenance
		 */
		private JTextField textRechercheMaintenance =new JTextField();
	 
    /**
     * Constructeur du panel qui prend en param�tre l'instance de fenetre en cours et qui va donc pouvoir changer de content pane grace a la m�thode switch panel
     * @param mainFrame
     */
public GUIMaintenance(GUI mainFrame) {
    	this.mainFrame=mainFrame;
    	this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

	/*Met en page, création du conteneur
	 choix du Layout pour ce conteneur
	 il permet de gérer la position des éléments
	 il autorisera un retaillage de la fenêtre en conservant la
	 présentation
	 BoxLayout permet par exemple de positionner les élements sur une
	 colonne ( PAGE_AXIS )*/
	this.setLayout(new BorderLayout());
		//Disposition des éléments composants
	
	/**
	 * Initialisation des boutons
	 */
	boutonCreation = new JButton("Cr�er une maintenance");
	boutonRecherche = new JButton("Rechercher une maintenance");
	boutonSupprimer=new JButton("Supprimer");
	boutonRetour=new JButton("Retour");

/**
 * Instanciations des fen�tres
 */
	pRetour=new JPanel();
	pRetour.setLayout(new BorderLayout());
	
	/**
	 * Onglet gestion Maintenance
	 */

	pOngletGestion=new JPanel();
	pOngletGestion.setLayout(new BoxLayout(pOngletGestion, BoxLayout.Y_AXIS));
	
	//Tableau
	
	pZoneTableau=new JPanel();

	pZoneTableau.setMaximumSize(getMaximumSize());
	
	//Recherche
	
	pRecherche=new JPanel();
	pRecherche.setLayout(new BorderLayout());
	pRecherche.add(jlRecherche, BorderLayout.PAGE_START);
	//pRecherche.add(Box.createRigidArea(new Dimension(0,5)));
	pRecherche.add(textRechercheMaintenance, BorderLayout.CENTER);
	//pRecherche.add(Box.createRigidArea(new Dimension(0,5)));
	
		
	/**
	 * Onglet Creation Maintenance
	 */
	pOngletCrea=new GUICreationMaintenance(mainFrame);
	/*pOngletCrea= new JPanel();
	pOngletCrea.setLayout(new BorderLayout());
	pOngletCrea.add(pCrea, BorderLayout.CENTER);
	*/
	
	/**
	 * Onglet Suppression Maintenance
	 */
	pOngletSupp=new JPanel();
	pOngletSupp.setLayout(new BoxLayout(pOngletSupp, BoxLayout.Y_AXIS));
	
	
	/**
	 * Ajout des Onglets
	 */
	tabbedPane = new JTabbedPane();
	//Ajout Onglet Gestion
	tabbedPane.addTab("Gestion Maintenance", pOngletGestion);
	tabbedPane.setMnemonicAt(0, KeyEvent.VK_1);
	
	//Ajout Onglet Creation
	tabbedPane.addTab("Cr�ation Maintenance",pOngletCrea);
	tabbedPane.setMnemonicAt(0, KeyEvent.VK_2);
	
	//Ajout Onglet Suppression
	tabbedPane.addTab("Suppression Maintenance", pOngletSupp);
	tabbedPane.setMnemonicAt(0, KeyEvent.VK_3);
		
/**
 * Donn�es du Tableau
 */
	 Object[][] data = {

		      {"Cysboy", "28 ans", "1.80 m","progrr","comm"},

		      {"BZHHydde", "28 ans", "1.80 m","progrr","comm"},

		      {"IamBow", "24 ans", "1.90 m","progrr","comm"},

		      {"FunMan", "32 ans", "1.85 m","progrr","comm"}

		    };
/**
 * Les titres des colonnes
 */

	    String  title[] = {"Nom", "Type", "Date","Progression","Commentaires"};
	    tableau = new JTable(data, title);
	    pZoneTableau.add(new JScrollPane(tableau));
	    
	 
/**
 * Ajout des boutons 	
 */
	//this.add(Box.createRigidArea(new Dimension(0,50)));   // Ajouter le composant au panel
	//this.add(boutonCreation, BorderLayout.NORTH);
	//this.add(Box.createRigidArea(new Dimension(0,50)));   // Ajouter le composant au panel
	pRecherche.add(boutonRecherche, BorderLayout.PAGE_END);
	//this.add(Box.createRigidArea(new Dimension(0,50)));   // Ajouter le composant au panel
	pRetour.add(boutonRetour, BorderLayout.PAGE_END);

/**
 * Ajout des Panels	
 */
	this.add(tabbedPane, BorderLayout.CENTER);
	pZoneTableau.setBorder(BorderFactory.createTitledBorder("Liste des maintenances"));
	pOngletGestion.add(pZoneTableau);
	pOngletGestion.add(pRecherche);
	this.add(pRetour, BorderLayout.PAGE_END);
	
/**
 * Ajout des Listener
 */	
	boutonCreation.addActionListener(this);
	boutonRecherche.addActionListener(this);
	boutonRetour.addActionListener(this);
	
    }

    
    
	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		if (ae.getSource() == boutonCreation) {
			this.mainFrame.switchPanel("Creation Maintenance");
		}
		else if (ae.getSource()==boutonRecherche){
			//this.mainFrame.switchPanel("Menu saisie client");
		}
		else if (ae.getSource()==boutonRetour){
			this.mainFrame.switchPanel(this.mainFrame.getRetour());
		}
		this.mainFrame.setRetour("Menu saisie maintenance");
	}

}
