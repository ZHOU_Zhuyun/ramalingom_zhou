import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;

public class GUIClient extends JPanel implements  ActionListener {
	private static final long serialVersionUID = 1L;
	
	private GUI mainFrame;
	
	private JButton boutonRechercheClient;
	private JButton boutonAjoutClient;
	private JButton boutonSuppressionClient;
	private JButton boutonRetour;
	
	public GUIClient(GUI mainFrame){
		this.mainFrame=mainFrame;
		
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		//this.setBackground(Color.); couleur arri�re plan
		
		boutonRechercheClient=new JButton("Rechercher un client");
		boutonAjoutClient=new JButton("Ajouter un client");
		boutonSuppressionClient=new JButton("Supprimer un client");
		boutonRetour=new JButton("Retour");
		
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonRechercheClient);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonAjoutClient);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonSuppressionClient);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonRetour);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		
		this.setBorder(BorderFactory.createEmptyBorder(20, 100, 20,20));
		boutonRechercheClient.addActionListener(this);
		boutonAjoutClient.addActionListener(this);
		boutonSuppressionClient.addActionListener(this);
		boutonRetour.addActionListener(this);
		
	}
		
	
	public void actionPerformed(ActionEvent ae) {
				
		if (ae.getSource() == boutonRechercheClient) {
			//this.mainFrame.switchPanel("Menu operateur");
		}
		else if (ae.getSource()==boutonAjoutClient){
			//this.mainFrame.switchPanel("Menu saisie client");
		}
		else if(ae.getSource()==boutonSuppressionClient){
			//this.mainFrame.switchPanel("Menu saisie maintenance");
		}
		else if(ae.getSource()==boutonRetour){
			this.mainFrame.switchPanel(this.mainFrame.getRetour());
		}
		this.mainFrame.setRetour("Menu saisie client");

	}	
}
