
import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;

public class GUIAdmin extends JPanel  implements ActionListener {
	private static final long serialVersionUID = 1L;
	private GUI mainFrame;
	
	private JButton boutonAfficherOperateur;
	private JButton boutonDeconnexion;
	
	public GUIAdmin(GUI mainFrame){
		this.mainFrame=mainFrame;
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		//this.setBackground(Color.); couleur arri�re plan
		
		boutonAfficherOperateur=new JButton("Afficher les Op�rateurs");
		boutonDeconnexion=new JButton("D�connection");
		
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonAfficherOperateur);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.add(boutonDeconnexion);
		this.add(Box.createRigidArea(new Dimension(0, 50)));
		this.setBorder(BorderFactory.createEmptyBorder(20, 100, 20,20));
		
		boutonAfficherOperateur.addActionListener(this);
		boutonDeconnexion.addActionListener(this);
	}
	
	
	
	public void actionPerformed(ActionEvent ae) {
		this.mainFrame.setRetour("Chef de Maintenance");
		
		if (ae.getSource() == boutonAfficherOperateur) {
			//this.mainFrame.switchPanel("Menu operateur");
		}
		else if (ae.getSource()==boutonDeconnexion){
			this.mainFrame.switchPanel("connection");
		}

	}	

}
