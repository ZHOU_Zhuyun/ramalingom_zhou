import java.util.ArrayList;
public class EntrepriseMaintenance {
	
protected String nomEntrepriseMaitenance;
protected ArrayList<Client> clients;
protected ArrayList<Operateur> operateurs;
//protected String nomChefMaintenance;

EntrepriseMaintenance(String nomEntrepriseMaintenance,ArrayList<Operateur> operateurs,ArrayList<Client> clients){
	this.nomEntrepriseMaitenance=nomEntrepriseMaintenance;
	this.operateurs=operateurs;
	this.clients=clients;
	//this.nomChefMaintenance=nomChefMaintenance;
}


// addall et removeall pour ajouter ou supprimer un liste d'element a une autre liste
//Getteur et Setteur
public String getNomEntrepriseMaitenance() {
	return nomEntrepriseMaitenance;
}

public void setNomEntrepriseMaitenance(String nomEntrepriseMaitenance) {
	this.nomEntrepriseMaitenance = nomEntrepriseMaitenance;
}

public ArrayList<Client> getClients() {
	return clients;
}

public void ajouterClients(ArrayList<Client> clients){//ajoute une liste de clients
	this.clients.addAll(clients);
}

public void ajouterClients(Client client){//cas particulier d'un unique client. cas un peu inutile impl�menter� juste pour �viter les erreur de la manip
	this.clients.add(client);
}
		
public void setClients(ArrayList<Client> clients) {//remet a z�ro tous les clients
	this.clients = clients;
}

public void supprimerClients(ArrayList<Client> clients){//supprime une liste de clients
	this.clients.removeAll(clients);
}

public ArrayList<Operateur> getOperateurs() {
	return operateurs;
}

public void ajouterOperateurs(ArrayList<Operateur> operateurs){//ajoute une liste d'operateurs
	this.operateurs.addAll(operateurs);
}

public void ajouterOperateurs(Operateur operateur){//cas particulier d'un unique op. cas un peu inutile impl�menter� juste pour �viter les erreur de la manip
	this.operateurs.add(operateur);
}

public void setOperateurs(ArrayList<Operateur> operateurs) {
	this.operateurs = operateurs;
}
public void supprimerOperateurs(ArrayList<Operateur> operateurs){//supprime une liste de clients
	this.operateurs.removeAll(operateurs);
}

/*
public String getNomChefMaintenance() {
	return nomChefMaintenance;
}

public void setNomChefMaintenance(String nomChefMaintenance) {
	this.nomChefMaintenance = nomChefMaintenance;
}
*/

}
