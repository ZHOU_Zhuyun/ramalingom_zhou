import java.util.*;

/**
 * @author yuyu
 * @version 1.0
 */
public class Client {

	protected String nomEntreprise;
	protected String numSiret;
	protected String adressEntreprise;
	protected String codeApe;
	
	public Client(String nomEntreprise, String numSiret, String adressEntreprise, String codeApe) {
		this.nomEntreprise = nomEntreprise;
		this.numSiret = numSiret;
		this.adressEntreprise = adressEntreprise;
		this.codeApe = codeApe;
	}
	
	public void afficherClient() {
		System.out.println("<Client> nomEntreprise: " + nomEntreprise + ", numSiret: " + numSiret + 
				", adressEntreprise: " + adressEntreprise + ", codeApe: " + codeApe);
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumSiret() {
		return numSiret;
	}

	public void setNumSiret(String numSiret) {
		this.numSiret = numSiret;
	}

	public String getAdressEntreprise() {
		return adressEntreprise;
	}

	public void setAdressEntreprise(String adressEntreprise) {
		this.adressEntreprise = adressEntreprise;
	}

	public String getCodeApe() {
		return codeApe;
	}

	public void setCodeApe(String codeApe) {
		this.codeApe = codeApe;
	}
	
}
