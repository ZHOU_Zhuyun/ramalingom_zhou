import java.sql.Date;

/**
 * @author yuyu
 * @version 1.0
 */
public class Devis {// Ici composition donc pas de extends d'h�ritage
	

	private String nomDevis;
	private String typeDeMaintenance;
	private Date date;
	private double prix;
	private Client client;
	

	public Devis(String nomDevis, String typeDeMaintenance, Date date, double prix,Client client) {
		this.nomDevis = nomDevis;
		this.typeDeMaintenance = typeDeMaintenance;
		this.date = date;
		this.prix = prix;
		this.client=client;
	}
	
	public String getNomDevis() {
		return nomDevis;
	}
	
	public Client getNomClient(){
		return client;
	}
	
	public void setClient(Client client){
		this.client=client;
	}
	
	public String getTypeDeMaintenance() {
		return typeDeMaintenance;
	}
	
	public Date getDate() {
		return date;
	}
	
	public double getPrix() {
		return prix;
	}
	
	public void modifierDevis(Date dateModif) {
		date = dateModif;
	}
	
	public void supprimerDevis(Devis devis) {			//?
		devis.nomDevis = "";
		devis.typeDeMaintenance = "";
	    devis.date = null;
		devis.prix = (Double) null;
	}
	
}
