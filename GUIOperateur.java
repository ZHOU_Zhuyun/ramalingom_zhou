import java.awt.FlowLayout;   // Emplacement des composants:Droite à gauche, haut en bas
import java.awt.GridLayout;

import javax.swing.JFrame;   //Bibliothèque permettant la modélisation graphique
import javax.swing.JLabel;   //Zone d'affichage
import javax.swing.JPanel;   // Bibliothèque du panel

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;

public class GUIOperateur extends JPanel  implements ActionListener {
	private static final long serialVersionUID = 1L;	
	
	private GUI mainFrame;
	
	//Paneaux pour le bouton Deconnexion
			private JPanel pCo;
	//Panneaux pour coller les panneaux
			private JPanel pFond;
			
	private JButton boutonModifDevis;
	private JButton boutonEditionRapportActivite;
	private JButton boutonModifFicheMaintenance;
	private JButton boutonDeconnexion;
	
	
	public GUIOperateur(GUI mainFrame){
		this.mainFrame=mainFrame;
		this.setLayout(new BorderLayout());
		//this.setBackground(Color.); couleur arri�re plan
		pFond=new JPanel();
		pFond.setLayout(new GridLayout( 3, 1, 5, 5));
		pCo=new JPanel();
		pCo.setLayout(new BorderLayout());
		
		boutonModifDevis=new JButton("Modifier un devis");
		boutonEditionRapportActivite=new JButton("Editer un rapport d'activit�");
		boutonModifFicheMaintenance=new JButton("Modifier une fiche de maintenance");
		boutonDeconnexion=new JButton("D�connection");
		
		//this.add(Box.createRigidArea(new Dimension(0, 50)));
		pFond.add(boutonModifDevis);
		//this.add(Box.createRigidArea(new Dimension(0, 50)));
		pFond.add(boutonEditionRapportActivite);
		//this.add(Box.createRigidArea(new Dimension(0, 50)));
		pFond.add(boutonModifFicheMaintenance);
		pCo.add(Box.createRigidArea(new Dimension(0, 20)));
		
		
		pCo.add(boutonDeconnexion, BorderLayout.PAGE_END);
		
	
		boutonModifDevis.addActionListener(this);
		boutonEditionRapportActivite.addActionListener(this);
		boutonModifFicheMaintenance.addActionListener(this);
		boutonDeconnexion.addActionListener(this);
		
		this.add(pFond, BorderLayout.CENTER);
		this.add(pCo, BorderLayout.PAGE_END);
		
		this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20,20));
			
		
	}
	
	
	public void actionPerformed(ActionEvent ae) {
			
				
		if (ae.getSource() == boutonModifDevis) {
			//this.mainFrame.switchPanel("Menu operateur");
		}
		else if (ae.getSource()==boutonEditionRapportActivite){
			//this.mainFrame.switchPanel("Menu saisie client");
		}
		else if(ae.getSource()==boutonModifFicheMaintenance){
			//this.mainFrame.switchPanel("Menu saisie maintenance");
		}
		else if(ae.getSource()==boutonDeconnexion){
			this.mainFrame.switchPanel("connection");
		}
		this.mainFrame.setRetour("Menu operateur");
	}	

}


